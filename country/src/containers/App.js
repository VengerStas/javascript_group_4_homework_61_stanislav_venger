import React, { Component } from 'react';
import  axios from 'axios';
import './App.css';
import CountryList from "../components/CoutryList/CountryList";
import CountryInfo from "../components/CountryInfo/CountryInfo";

class App extends Component {
    state = {
        countries: [],
        countryInfo: [],
        borders: []
    };

    countryHandler = code => {
        const COUNTRY_URL = 'alpha/';
        axios.get(COUNTRY_URL + code).then(response => {
            this.setState({countryInfo: response.data});
            return response;
        }).then(response => {
            if (response.data.borders.length !== 0) {
                const border = [];
                Promise.all(response.data.borders.map(alpha => {
                    return axios.get(COUNTRY_URL + alpha).then(country => {
                        const borderName = country.data.name;
                        border.push(borderName);
                        this.setState({borders: border});
                    })
                }))
            } else {
                this.setState({borders: []})
            }
        }).catch(error => {
            console.log(error);
        })
    };

    componentDidMount () {
        const ALL_URL = 'all?fields=name;alpha3Code';

        axios.get(ALL_URL).then(response => {
            const countries = response.data;
            this.setState({countries});
        }).catch(error => {
            console.log(error);
        })
    }

    componentDidUpdate () {
        this.countryHandler();
    }

  render() {
        const currentCountry = this.state.countryInfo;
    return (
      <div className="App">
            <div className="list-block">
                {
                    this.state.countries.map((country, index) => {
                        return (
                            <CountryList
                                key={country.name}
                                country={country.name}
                                countryHandler={() => this.countryHandler(country.alpha3Code)}
                            />
                        )
                    })
                }
            </div>
            <div className="info-block">
               <CountryInfo
                   borders={this.state.borders}
                   currentCountry={currentCountry.name}
                   capital={currentCountry.capital}
                   population={currentCountry.population}
                   imgSrc={currentCountry.flag}
               />
            </div>
      </div>
    );
  }
}

export default App;
