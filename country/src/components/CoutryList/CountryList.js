import React from 'react';
import './CountryList.css';
const CountryList = props => {
    return (
        <div>
            <p className="country" onClick={props.countryHandler}>{props.country}</p>
        </div>
    );
};

export default CountryList;