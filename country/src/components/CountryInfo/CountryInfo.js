import React from 'react';
import './CountryInfo.css';
const CountryInfo = props => {
    return (
        <div className="country-info-block">
            <h2 className="name">{props.currentCountry}</h2>
            <p className="capital"><strong>Capital: </strong>{props.capital}</p>
            <p className="population"><strong>Population: </strong>{props.population}</p>
            <img className="flag-img" src={props.imgSrc} alt={props.currentCountry}/>
            {
                props.borders !== [] ?  <ul>
                    {
                        props.borders.map((border, index) => {
                            return (
                                <li key={index}>
                                    {border}
                                </li>
                            )
                        })
                    }
                </ul> : null
            }
        </div>
    );
};

export default CountryInfo;